﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class MessageProgram : IProgram
    {
        private string name;

        public MessageProgram()
        {
            name = "Message";
        }

        public void Run()
        {
            Console.WriteLine("Message Run");
        }

        public string Name
        {
            get { return name; }
        }
    }
}
