﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class CallProgram : IProgram
    {
        private string name;

        public CallProgram()
        {
            name = "Call";
        }

        public void Run()
        {
            Console.WriteLine("Call Run");
        }

        public string Name
        {
            get { return name; }
        }
    }
}
