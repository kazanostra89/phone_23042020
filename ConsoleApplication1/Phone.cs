﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    class Phone
    {
        private string namePhone;
        private string modelPhone;
        private List<IProgram> programs;

        public Phone(string namePhone, string modelPhone)
        {
            this.namePhone = namePhone;
            this.modelPhone = modelPhone;

            programs = new List<IProgram>();
        }

        public void InstallProgram(IProgram program)
        {
            programs.Add(program);
        }

        public void PrintInfo()
        {
            Console.WriteLine("Имя телефона: " + namePhone + "Модель телефона: " + modelPhone);

            if (programs.Count == 0)
            {
                Console.WriteLine("На телефоне отсутствуют установленные программы");
                return;
            }

            foreach (IProgram item in programs)
            {
                Console.WriteLine(item.Name);
            }
        }

        public void RunProgram(string nameProgram)
        {
            /*bool notFound = true;

            for (int i = 0; i < programs.Count; i++)
            {
                if (programs[i].Name == nameProgram)
                {
                    programs[i].Run();
                    notFound = false;
                }
            }

            if (notFound)
            {
                throw new Exception("Error! Not Found program");
            }*/

            IProgram programFind = programs.Find(item => item.Name == nameProgram);

            if (programFind != null)
            {
                programFind.Run();
            }
            else
            {
                throw new Exception("Error! Not Found program");
            }

        }

        public bool DeleteProgram(string nameProgram)
        {
            return programs.Remove(programs.Find(item => item.Name == nameProgram));
            
        }

    }
}
