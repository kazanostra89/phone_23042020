﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void PrintMenu()
        {
            Console.WriteLine("1. Установить программу");
            Console.WriteLine("2. Удалить программу");
            Console.WriteLine("3. Запустить программу");
            Console.WriteLine("0. Выход");

        }

        static void PrintInstallProgram()
        {
            Console.WriteLine("1. Установить змейку");
            Console.WriteLine("2. Установить звонилку");
            Console.WriteLine("3. Установить сообщалку");
        }

        static void Main(string[] args)
        {
            
            Phone phone = new Phone("Nokia", "8800");
            bool runProgram = true;
            int selectMenu;
            bool checkSelect;

            while (runProgram)
            {
                Console.Clear();
                phone.PrintInfo();
                PrintMenu();

                selectMenu = InputInt(0, 3, "Введите пункт меню: ");

                switch (selectMenu)
                {
                    case 1:
                        PrintInstallProgram();

                        int selectInstallProgram = InputInt(1, 3, "Выберите доступные для установки программы: ");

                        switch (selectInstallProgram)
                        {
                            case 1:
                                phone.InstallProgram(new SnakeProgram());
                                break;
                            case 2:
                                phone.InstallProgram(new CallProgram());
                                break;
                            case 3:
                                phone.InstallProgram(new MessageProgram());
                                break;
                        }

                        break;
                    case 2:
                        Console.Write("Введите имя удаляемой программы: ");
                        string nameProgram = Console.ReadLine();

                        if (phone.DeleteProgram(nameProgram))
                        {
                            Console.WriteLine("Программа удалена!");
                        }
                        else
                        {
                            Console.WriteLine("Такой программы нет");
                        }

                        Console.ReadKey();
                        break;
                    case 3:
                        Console.Write("Введите имя запускаемой программы: ");
                        string runInputProgram = Console.ReadLine();

                        try
                        {
                            phone.RunProgram(runInputProgram);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }

                        Console.ReadKey();
                        break;
                    case 0:
                        runProgram = false;
                        break;
                }

            }

            
        }

        static int InputInt(int min, int max, string message)
        {
            int selectMenu;
            bool checkSelect;

            do
            {
                Console.Write(message);
                checkSelect = int.TryParse(Console.ReadLine(), out selectMenu);

            } while (checkSelect == false || selectMenu < min || selectMenu > max);

            return selectMenu;
        }
    }
}
