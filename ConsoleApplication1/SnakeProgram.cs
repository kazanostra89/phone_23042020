﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class SnakeProgram : IProgram
    {
        private string name;

        public SnakeProgram()
        {
            name = "Snake";
        }

        public void Run()
        {
            Console.WriteLine("Snake Run");
        }

        public string Name
        {
            get { return name; }
        }
    }
}
